# Uber Publisher AMP

Accelerated Mobile Pages (AMP) integration module.

This is an [Uber Publisher](https://www.drupal.org/project/uber_publisher)
 feature.

For better experince enable simple_amp module, it's not as dependency to make
 it more flexible in case anyone doesn't prefer to enable it.

Join Our Slack Team for Feedback and Support 
http://slack.varbase.vardot.com/

This module is sponsored and developed by [Vardot](https://www.drupal.org/vardot).
